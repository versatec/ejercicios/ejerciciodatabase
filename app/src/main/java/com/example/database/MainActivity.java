package com.example.database;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

//Actividad Principal
public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    FloatingActionButton add_button;
    ImageView empty_imageView;
    TextView no_data;

    MyDatabaseHelper myDB;
    ArrayList<String> moneta_id, nominal_coin, year_coin, price_coin ;
    CustomAdapter customAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView); //Lista
        add_button = findViewById(R.id.add_button);
        empty_imageView = findViewById(R.id.empty_imageView);
        no_data = findViewById(R.id.noData);

        //cambiar a la actividad "agregar a la base de datos"
        add_button.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, AddActivity.class);
            startActivity(intent);
        });

        myDB = new MyDatabaseHelper(MainActivity.this);
        moneta_id = new ArrayList<>();
        nominal_coin = new ArrayList<>();
        year_coin = new ArrayList<>();
        price_coin = new ArrayList<>();

        viewDataInArrays();

        //qué muestra correctamente los datos en recyclerView
        customAdapter = new CustomAdapter(MainActivity.this,this, moneta_id, nominal_coin, year_coin, price_coin);
        recyclerView.setAdapter(customAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

    }

    //si los datos se actualizan, entonces la actividad principal también se actualiza
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            recreate();
        }
    }

    //enviar datos a una matriz
    void viewDataInArrays(){
       Cursor cursor = myDB.readAllData();
        if (cursor.getCount() == 0){
            empty_imageView.setVisibility(View.VISIBLE);
            no_data.setVisibility(View.VISIBLE);
        }else{
            while (cursor.moveToNext()){
                moneta_id.add(cursor.getString(0));
                nominal_coin.add(cursor.getString(1));
                year_coin.add(cursor.getString(2));
                price_coin.add(cursor.getString(3));
            }
            empty_imageView.setVisibility(View.GONE);
            no_data.setVisibility(View.GONE);

        }
    }

    //mostrar el botón eliminar todo
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //¿Qué sucede cuando haces clic en eliminar todo?
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.delete_all){
            confirmDialog();
        }

        return super.onOptionsItemSelected(item);

    }
    //cuadro de diálogo de confirmación de eliminación
    void confirmDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Eliminar todo ?");
        builder.setMessage("¿Estás seguro de que deseas eliminar todos los datos?");
        builder.setPositiveButton("Sí", (dialog, which) -> {
            MyDatabaseHelper myDB = new MyDatabaseHelper(this);
            myDB.deleteAllData();
            //para visualizar sin problemas sin pantallas negras, recrea la actividad
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        });
        builder.setNegativeButton("No", (dialog, which) -> {

        });
        builder.create().show();
    }
}