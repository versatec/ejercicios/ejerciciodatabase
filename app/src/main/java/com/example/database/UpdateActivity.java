package com.example.database;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//actualizar
public class UpdateActivity extends AppCompatActivity {

    EditText nominal_input, year_input, price_input;
    Button update_button, delete_button;

    String id, nominal, year, price;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        nominal_input = findViewById(R.id.nominal_input2);
        year_input = findViewById(R.id.year_input2);
        price_input = findViewById(R.id.price_input2);
        update_button = findViewById(R.id.update_button);
        delete_button = findViewById(R.id.delete_button);

        //1) Primero recopilamos e instalamos datos.
        getAndSetIntentData();


        //establecer título después getAndSetIntentData()
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(nominal);
        }
        update_button.setOnClickListener(v -> {

            //2) Luego los actualizamos
            MyDatabaseHelper myDB = new MyDatabaseHelper(UpdateActivity.this);
            nominal = nominal_input.getText().toString().trim();
            year = year_input.getText().toString().trim();
            price = price_input.getText().toString().trim();
            myDB.updateData(id, nominal, year, price);
        });

        delete_button.setOnClickListener(v -> {
            confirmDialog();
        });



    }

    //obtener y configurar datos de la intención
    void getAndSetIntentData(){
        if(getIntent().hasExtra("id") && getIntent().hasExtra("nominal") && getIntent().hasExtra("year") && getIntent().hasExtra("price")){

            //tomar datos de la intención
            id = getIntent().getStringExtra("id");
            nominal = getIntent().getStringExtra("nominal");
            year = getIntent().getStringExtra("year");
            price = getIntent().getStringExtra("price");

            //instalar datos desde la intención
            nominal_input.setText(nominal);
            year_input.setText(year);
            price_input.setText(price);

        }else{
            Toast.makeText(this, "No data", Toast.LENGTH_SHORT).show();
        }
    }

    //cuadro de diálogo de confirmación de eliminación
    void confirmDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Borrar "+"\""+nominal+"\""+" ?");
        builder.setMessage("estas seguro que quieres borrarlo " + "\""+nominal +"\""+ " ?");
        builder.setPositiveButton("Si", (dialog, which) -> {
            MyDatabaseHelper myDB = new MyDatabaseHelper(UpdateActivity.this);
            myDB.deleteOneRow(id);
            //transición suave sin pantallas negras
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();

        });
        builder.setNegativeButton("No", (dialog, which) -> { });
        builder.create().show();
    }
}